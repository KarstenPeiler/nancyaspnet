﻿using Nancy;

namespace TestProjekt.Modules
{
    public class MainModule: NancyModule
    {
        public MainModule()
        {
            Get["/"] = _ =>
            {
                return View["Startseite.html"];
            };

            Put["/"] = _ =>
            {
                return View["Antwortseite.html"];
            };
        }
    }
}